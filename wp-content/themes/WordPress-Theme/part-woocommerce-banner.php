<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php
				$category_1_id = 17;
				$category_1 = 'anillos-de-compromiso';
				if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_anillos_de_compromiso' );
				}
				$category_2_id = 19;
				$category_2 = 'aretes';
				if ( is_product_category( $category_2 ) || has_term( $category_2, 'product_cat' ) || term_is_ancestor_of( $category_2_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_aretes' );
				}
				$category_3_id = 18;
				$category_3 = 'argollas-de-boda';
				if ( is_product_category( $category_3 ) || has_term( $category_3, 'product_cat' ) || term_is_ancestor_of( $category_3_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_argollas_de_boda' );
				}
				$category_4_id = 20;
				$category_4 = 'cadenas';
				if ( is_product_category( $category_4 ) || has_term( $category_4, 'product_cat' ) || term_is_ancestor_of( $category_4_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_cadenas' );
				}
				$category_5_id = 23;
				$category_5 = 'collares';
				if ( is_product_category( $category_5 ) || has_term( $category_5, 'product_cat' ) || term_is_ancestor_of( $category_5_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_collares' );
				}
				$category_6_id = 21;
				$category_6 = 'dijes';
				if ( is_product_category( $category_6 ) || has_term( $category_6, 'product_cat' ) || term_is_ancestor_of( $category_6_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_dijes' );
				}
				$category_7_id = 22;
				$category_7 = 'pulseras';
				if ( is_product_category( $category_7 ) || has_term( $category_7, 'product_cat' ) || term_is_ancestor_of( $category_7_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_pulseras' );
				}
				?>
			</div>
		</div>
	</section>
<!-- End Banner -->